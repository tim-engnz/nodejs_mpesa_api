const express = require('express')
const {MpesaHandler} = require("./mpesa/MpesaHandler");
const app = express()
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const bodyParser = require('body-parser');
var o2x = require('object-to-xml');
require('body-parser-xml')(bodyParser);


app.use(helmet());
app.use(cors());
app.use(compress());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.get('/', (req, res) => {
    res.send('Hello World!')
})
app.post('/', (req, res) => {
    res.send('Success')
})

app.get('/request_stk', (req, res) => {

    new MpesaHandler().send_stk(req.query)
        .then(results => {
            res.status(200).json(results)
        }).catch(err => {
        console.error(err)
        res.status(500).json({
            ResponseCode: 55,
            ResponseDescription: err.message
        })

    })

})

app.get('/query', (req, res) => {

    new MpesaHandler().query(req.query)
        .then(results => {
            res.status(200).json(results)
        }).catch(err => {
        console.error(err)
        res.status(500).json({
            ResponseCode: 55,
            ResponseDescription: err.message
        })

    })

})


app.post('/request_stk', (req, res) => {


    new MpesaHandler().send_stk(req.body)
        .then(results => {
            res.status(200).json(results)
        }).catch(err => {
        res.status(500).json({
            ResponseCode: 55,
            ResponseDescription: err.message
        })

    })
})


app.post('/api-internal/mpesastk-results', (req, res) => {
    console.log("/api-internal/mpesastk-results")
    console.log(JSON.stringify(req.body))
    new MpesaHandler().stk_results(req.body)
        .then(results => {
            res.status(200).json(results)
        }).catch(err => {
        console.error(err)
        res.status(500).json({
            ResponseCode: 55,
            ResponseDescription: err.message
        })

    })
})

app.post('/api-internal/mpesa-validation', (req, res) => {
    console.log("/api-internal/mpesa-validation")
    console.log(JSON.stringify(req.body))
    new MpesaHandler().mpesa_validation(req.body)
        .then(results => {
            res.status(200).json(results)
        }).catch(err => {
        console.error(err)
        res.status(500).json({
            ResponseCode: 55,
            ResponseDescription: err.message
        })

    })
})

app.post('/api-internal/mpesa-confirmation', (req, res) => {
    console.log("/api-internal/mpesa-confirmation")
    console.log(JSON.stringify(req.body))
    new MpesaHandler().mpesa_confirmation(req.body)
        .then(results => {
            res.status(200).json(results)
        }).catch(err => {
        console.error(err)
        res.status(500).json({
            ResponseCode: 55,
            ResponseDescription: err.message
        })

    })
})


let port = 3000
app.listen(port, () => {
    console.log(`Mpesa API app listening at http://localhost:${port}`)
})
