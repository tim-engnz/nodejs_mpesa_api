const moment = require('moment');
const request = require('request');
var dateFormat = require('dateformat');
const randomstring = require("randomstring");
const {Sequelize, DataTypes, Op} = require('sequelize');
const model_fxn = require('./../models/mpesa_api.model');
const { v4: uuidv4 } = require('uuid');


process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;

let global_config = require('../config.json')
let tempConfig = global_config['development'] || {}
this.config = tempConfig['mpesa']

let {username, password, host, port, dialect, db_name} = tempConfig['database']
const sequelize = new Sequelize(db_name, username, password, {
    host: host,
    dialect: dialect
});

let MpesaApiModel = model_fxn(sequelize, DataTypes)
sequelize.authenticate().then(console.log).catch(console.error)


exports.MpesaHandler = class MpesaHandler {
    constructor() {
        let tempConfig = global_config['development'] || {}
        this.config = tempConfig['mpesa']
    }


    async query(query={}){
        return await MpesaApiModel.findAll({
            where: query
        })
    }

    async mpesa_confirmation(request_obj = {}) {


        // Check if there is a previous stk_request with similar request
        let existingTrx = await MpesaApiModel.findOne({
            where: {
                amount: request_obj.TransAmount,
                account_reference: request_obj.BillRefNumber,
                fulfilment_status: 'PENDING',
                Amount: request_obj.TransAmount,

                //complete withing 1 minute
                createdAt: {
                    [Op.gte]: moment().subtract(1, 'minutes').toDate()
                },

                //If using v2, disable phone number check
                phoneNumber: request_obj.MSISDN,
            }
        })

        if (existingTrx) {
            let m =await existingTrx.update({
                fulfilment_mode:'CONFIRMATION',
                fulfilment_status: 'SUCCESS',
                transaction_date: request_obj.TransTime,
                fulfilment_trxid: request_obj.TransID,
                raw_payload: JSON.stringify(request_obj),
            })

            return {
                "ResponseCode": "0",
                "ResponseDescription": "Success"
            }

        } else {
            let m = await MpesaApiModel.create({
                uuid:uuidv4(),
                fulfilment_mode:'CONFIRMATION',
                shortcode: request_obj.BusinessShortCode,
                phoneNumber: request_obj.MSISDN,
                amount: request_obj.TransAmount,
                account_reference: request_obj.BillRefNumber,
                description: `${request_obj.FirstName || ''} ${request_obj.MiddleName || ''} ${request_obj.LastName || ''}`,
                fulfilment_status: 'SUCCESS',
                transaction_date: request_obj.TransTime,
                fulfilment_trxid: request_obj.TransID,
                raw_request: JSON.stringify({}),
                raw_payload: JSON.stringify(request_obj)
            })

            return {
                "ResponseCode": "0",
                "ResponseDescription": "Success",
                "UniqueId":m.uuid
            }
        }
    }

    async mpesa_validation(request_obj = {}) {

        if (request_obj.TransAmount > 5) {
            return {
                "ResponseCode": "0",
                "ResponseDescription": "Success"
            }
        } else {
            return {
                "ResponseCode": "55",
                "ResponseDescription": "Transaction Declined"
            }
        }
    }

    async stk_results(request_obj = {}) {

        let stkCallback = {
            ...request_obj.Body.stkCallback, ...request_obj.Body.stkCallback.CallbackMetadata.Item.reduce((obj, item) => {
                obj[item['Name']] = item['Value'] || '';
                return obj
            }, {})
        }


        // Check if there is a previous fulfilment
        let existingTrx = await MpesaApiModel.findOne({
            where: {
                CheckoutRequestID: stkCallback.CheckoutRequestID,
            }
        })

        if (existingTrx) {
            let m =await existingTrx.update({
                fulfilment_mode:'STK',
                fulfilment_status: 'SUCCESS',
                fulfilment_trxid: stkCallback.MpesaReceiptNumber,
                raw_payload: JSON.stringify(stkCallback),
                transaction_date: stkCallback.TransactionDate
            })

            return {
                "ResponseCode": "0",
                "ResponseDescription": "Success",
                "UniqueId":m.uuid
            }

        } else {
            let m = await MpesaApiModel.create({
                uuid:uuidv4(),
                fulfilment_mode:'STK',
                phoneNumber: request_obj.PhoneNumber,
                shortcode: stkCallback.BusinessShortCode,
                amount: stkCallback.TransAmount,
                account_reference: stkCallback.BillRefNumber,
                description: `${stkCallback.FirstName || ''} ${stkCallback.MiddleName || ''} ${stkCallback.LastName || ''}`,
                fulfilment_status: 'SUCCESS',
                fulfilment_trxid: stkCallback.TransID,
                transaction_date: stkCallback.TransactionDate,
                raw_request: JSON.stringify({}),
                raw_payload: JSON.stringify(stkCallback)
            })

            return {
                "ResponseCode": "0",
                "ResponseDescription": "Success",
                "UniqueId":m.uuid
            }
        }

    }


    async send_stk(request_obj = {}) {
        // console.log('===========',request_obj)
        if (!(request_obj.amount || request_obj.phoneNumber || request_obj.accountReference || request_obj.description)) {
            throw Error('Invalid request_obj received')
        } else {
            let BusinessShortCode = this.config['shortcode']
            let callbackUrl = this.config['stk_callback_url']
            let timeStamp = moment().format('YYYYMMDDHHmmss')
            // let timeStamp = this.config['timestamp']
            let passkey = this.config['passkey']
            let rawPass = BusinessShortCode + passkey + timeStamp

            // Request object
            let stkMpesaTransaction = {
                BusinessShortCode: BusinessShortCode,
                Password: Buffer.from(rawPass).toString('base64'),
                Timestamp: timeStamp,
                TransactionType: 'CustomerPayBillOnline',
                Amount: request_obj.amount,
                PartyA: request_obj.phoneNumber,
                PartyB: BusinessShortCode,
                PhoneNumber: request_obj.phoneNumber,
                CallBackURL: callbackUrl,
                AccountReference: request_obj.accountReference,
                TransactionDesc: request_obj.description
            }
            console.log(' POST Req: ' + JSON.stringify(stkMpesaTransaction))

            let accessToken = await this.get_valid_token()
            // console.log(`AccessToken: ${accessToken}`)

            const options = {
                'method': 'POST',
                'url': this.config['links']['stk_process_request'],
                'headers': {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${accessToken}`

                },
                timeout: 5000,
                body: JSON.stringify(stkMpesaTransaction)

            };
            // console.log(`Options: ${options}`)
            // console.log(options)

            return new Promise(((resolve, reject) => {
                request(options, async function (error, response) {
                    if (error) reject(error);
                    // console.debug(response);
                    try {
                        let response_json = JSON.parse(response.body)
                        let m = await MpesaApiModel.create({
                            uuid:uuidv4(),
                            phoneNumber: stkMpesaTransaction.PhoneNumber,
                            shortcode: stkMpesaTransaction.BusinessShortCode,
                            amount: stkMpesaTransaction.Amount,
                            account_reference: stkMpesaTransaction.AccountReference,
                            description: stkMpesaTransaction.TransactionDesc,
                            MerchantRequestID: response_json.MerchantRequestID,
                            CheckoutRequestID: response_json.CheckoutRequestID,
                            // fulfilment_status:BusinessShortCode,
                            // fulfilment_trxid:BusinessShortCode,
                            raw_request: JSON.stringify(stkMpesaTransaction),
                            raw_payload: response.body
                        })
                        // resolve({response_json})
                        resolve({
                            "ResponseCode": response_json.ResponseCode,
                            "ResponseDescription": response_json.ResponseDescription,
                            "UniqueId":m.uuid
                        })
                    } catch (error) {
                        reject(error)
                        // reject({
                        //     "ResponseCode": "5",
                        //     "ResponseDescription": error.message,
                        // })
                    }
                });
            }))
        }
    }

    async get_valid_token(serviceName = "STK_PUSH") {
        let consumerKey = 'YOUR_APP_CONSUMER_KEY'
        let consumerSecret = 'YOUR_APP_CONSUMER_SECRET'
        let token = {}
        let url = this.config['links']['token']

        switch (serviceName) {
            case "STK_PUSH": {
                consumerKey = this.config['stk_auth']['consumer_key']
                consumerSecret = this.config['stk_auth']['consumer_secret']
                break
            }
            case "C2B_URL_REGISTRATION": {
                consumerKey = this.config['c2b_register_auth']['consumer_key']
                consumerSecret = this.config['c2b_register_auth']['consumer_secret']
                break
            }
        }

        // console.log(`consumerKey: ${consumerKey}, consumerSecret: ${consumerSecret}`)


        // Combine consumer key with the secret
        let auth = 'Basic ' + Buffer.from(consumerKey + ':' + consumerSecret).toString('base64')
        // console.log(`Auth: ${auth}`)

        return new Promise(((resolve, reject) => {

            request({url: url, headers: {'Authorization': auth}},
                function (error, response, body) {
                    // Process successful token response
                    console.log(body)
                    let tokenResp = JSON.parse(body)

                    // Check if response contains error
                    if (!error || !tokenResp.errorCode) {
                        let newToken = {
                            lastUpdated: moment().format('YYYY-MM-DD HH:mm:ss'),
                            accessToken: tokenResp.access_token,
                            timeout: tokenResp.expires_in,
                            service: serviceName
                        }

                        resolve(tokenResp.access_token)

                    } else {
                        reject(error)
                    }
                })

        }))
    }


}
