# NodeJS MpesaAPI
Node version >= 10 but less that 12

##Setup
- Run <code>npm i</code>
- To start <code>node server.js</code>

## Running
- Uses Sequalize, Bu Default Mysql. Below is create table statement
    <code>
    --
    -- test.mpesa_api definition
    
    CREATE TABLE `mpesa_api` (
      `id` int NOT NULL AUTO_INCREMENT,
      `uuid` varchar(50) NOT NULL,
      `phoneNumber` varchar(15) NOT NULL,
      `shortcode` varchar(10) DEFAULT NULL,
      `amount` double NOT NULL,
      `account_reference` varchar(50) DEFAULT NULL,
      `description` text,
      `fulfilment_status` enum('PENDING','SUCCESS','FAILED') DEFAULT 'PENDING',
      `fulfilment_mode` enum('STK','CONFIRMATION') DEFAULT NULL,
      `fulfilment_trxid` varchar(20) DEFAULT NULL,
      `MerchantRequestID` varchar(50) DEFAULT NULL,
      `CheckoutRequestID` varchar(50) DEFAULT NULL,
      `transaction_date` varchar(20) DEFAULT NULL,
      `raw_request` text,
      `raw_payload` text,
      `createdAt` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
      `updatedAt` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
      PRIMARY KEY (`id`),
      UNIQUE KEY `fulfilment_trxid` (`fulfilment_trxid`),
      UNIQUE KEY `MerchantRequestID` (`MerchantRequestID`),
      UNIQUE KEY `CheckoutRequestID` (`CheckoutRequestID`)
    ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
    
    -
    </code>
- Be sure to change configs on <code>./config.json</code>
```json
{
  "development": {
      "database": {
        "username": "root",
        "password": "PASSWORD",
        "host": "localhost",
        "port": "3306",
        "db_name": "test",
        "dialect": "mysql"
      },
      "mpesa": {
        "shortcode":"11111",
        "stk_auth":{
          "consumer_key":"CK",
          "consumer_secret":"CK"
        },
        "c2b_register_auth":{
          "consumer_key":"CK",
          "consumer_secret":"CK"
        },
        "timestamp":"20180121060000",
        "passkey":"PASSKEY_HERE",
        "c2b_confirmation_url":"http://localhost:3000/api-internal/mpesa-confirmation",
        "c2b_validation_url":"http://localhost:3000/api-internal/mpesa-validation",
        "stk_callback_url":"http://localhost:3000/api-internal/mpesastk-results",
        "links": {
          "token":"http://localhost:5040/mpesa/oauth/v1/generate",
          "c2b_register":"http://localhost:5040/mpesa/stkpush/v1/register_url",
          "stk_process_request":"http://localhost:5040/mpesa/stkpush/v1/processrequest"

        }
     }
  }
}

```

## Apis to Note:
- To Request Payment:
```
curl -L -X GET 'http://localhost:3000/request_stk?phoneNumber=254705126329&amount=20&accountReference=TEST1&description=Test' -H 'd: '
```
Response:
```json
{
    "ResponseCode": 0,
    "ResponseDescription": "Success. Request accepted for processing",
    "UniqueId": "4538a15d-1705-44f3-88d3-93089ead27c2"
}
```

- To Query:

```
curl -L -X GET 'http://localhost:3000/query?uuid=4538a15d-1705-44f3-88d3-93089ead27c2'
```

Response:
```json
[
    {
        "id": 2,
        "uuid": "4538a15d-1705-44f3-88d3-93089ead27c2",
        "phoneNumber": "254705126329",
        "shortcode": "11111",
        "amount": 20,
        "account_reference": "TEST1",
        "description": "Test",
        "fulfilment_status": "SUCCESS",
        "fulfilment_mode": "STK",
        "fulfilment_trxid": "JCQQT7N",
        "MerchantRequestID": "45441-3643702-84",
        "CheckoutRequestID": "ws_WOS-02106604189751146491",
        "transaction_date": "2022071202065590",
        "raw_request": "{\"BusinessShortCode\":\"11111\",\"Password\":\"MTExMTFkOTFjNjZiODIzN2IyZGYyMjc4NmI0MTJlYjUxNzkzZWQ1MzZkMjJjNjk5MmJlYzA3MTk5M2ZmMjU1ODZlNzAyMjAyMjA3MTIwMjA2NTU=\",\"Timestamp\":\"20220712020655\",\"TransactionType\":\"CustomerPayBillOnline\",\"Amount\":\"20\",\"PartyA\":\"254705126329\",\"PartyB\":\"11111\",\"PhoneNumber\":\"254705126329\",\"CallBackURL\":\"http://localhost:3000/api-internal/mpesastk-results\",\"AccountReference\":\"TEST1\",\"TransactionDesc\":\"Test\"}",
        "raw_payload": "{\"MerchantRequestID\":\"45441-3643702-84\",\"CheckoutRequestID\":\"ws_WOS-02106604189751146491\",\"ResultCode\":\"0\",\"ResultDesc\":\"Success. Request accepted for processing\",\"CallbackMetadata\":{\"Item\":[{\"Name\":\"Amount\",\"Value\":\"20\"},{\"Name\":\"MpesaReceiptNumber\",\"Value\":\"JCQQT7N\"},{\"Name\":\"Balance\"},{\"Name\":\"TransactionDate\",\"Value\":\"2022071202065590\"},{\"Name\":\"PhoneNumber\",\"Value\":\"254705126329\"}]},\"Amount\":\"20\",\"MpesaReceiptNumber\":\"JCQQT7N\",\"Balance\":\"\",\"TransactionDate\":\"2022071202065590\",\"PhoneNumber\":\"254705126329\"}",
        "createdAt": "2022-07-11T23:06:55.000Z",
        "updatedAt": "2022-07-11T23:07:05.000Z"
    }
]
```
- Note, you can query by any of the fields provided or by combining multiple fields on the query params