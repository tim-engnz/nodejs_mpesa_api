/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('mpesa_api', {
    'id': {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      primaryKey: true,
      comment: "null",
      autoIncrement: true
    },
    'uuid': {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: "null"
    },
    'phoneNumber': {
      type: DataTypes.STRING(15),
      allowNull: false,
      comment: "null"
    },
    'shortcode': {
      type: DataTypes.STRING(10),
      allowNull: true,
      comment: "null"
    },
    'amount': {
      type: "DOUBLE",
      allowNull: false,
      comment: "null"
    },
    'account_reference': {
      type: DataTypes.STRING(50),
      allowNull: true,
      comment: "null"
    },
    'description': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'fulfilment_status': {
      type: DataTypes.ENUM('PENDING','SUCCESS','FAILED'),
      allowNull: true,
      defaultValue: 'PENDING',
      comment: "null"
    },
    'fulfilment_mode': {
      type: DataTypes.ENUM('STK','CONFIRMATION'),
      allowNull: true,
      comment: "null"
    },
    'fulfilment_trxid': {
      type: DataTypes.STRING(20),
      allowNull: true,
      comment: "null",
      unique: true
    },
    'MerchantRequestID': {
      type: DataTypes.STRING(50),
      allowNull: true,
      comment: "null",
      unique: true
    },
    'CheckoutRequestID': {
      type: DataTypes.STRING(50),
      allowNull: true,
      comment: "null",
      unique: true
    },
    'transaction_date': {
      type: DataTypes.STRING(20),
      allowNull: true,
      comment: "null"
    },
    'raw_request': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'raw_payload': {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: "null"
    },
    'createdAt': {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "null"
    },
    'updatedAt': {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "null"
    }
  }, {
    tableName: 'mpesa_api'
  });
};
